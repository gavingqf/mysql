#pragma once

/*
 * command queue.
 */

#include <list>
#include "gnmutex.h"
#include "mysql_db.h"

namespace SMysqlSpace {
	class CCommandQueue {
	public:
		CCommandQueue() {
			GNDP::CAutoLock<GNDP::CLocker> oLocker(m_mutex);
			m_listCommand.clear();
		}
		virtual ~CCommandQueue() {
			GNDP::CAutoLock<GNDP::CLocker> oLocker(m_mutex);
			m_listCommand.clear();
		}

	public:
		std::pair<bool, ICommand*> GetFront() {
			GNDP::CAutoLock<GNDP::CLocker> oLocker(m_mutex);
			if (!m_listCommand.empty()) {
				ICommand* poCommand = m_listCommand.front();
				m_listCommand.pop_front();
				return { true,poCommand };
			}
			return { false,nullptr };
		}

		void PushBack(ICommand* poCommand) {
			GNDP::CAutoLock<GNDP::CLocker> oLocker(m_mutex);
			m_listCommand.push_back(poCommand);
		}

		void PushFront(ICommand* poCommand) {
			GNDP::CAutoLock<GNDP::CLocker> oLocker(m_mutex);
			m_listCommand.push_front(poCommand);
		}

	protected:
		std::list<ICommand*> m_listCommand;
		GNDP::CLocker m_mutex;
	};
}