#pragma once

namespace SMysqlSpace {
	// 高效单向线程队列
	// 注意：使用时仅供一个读线程和一个写线程
	// 队列的元素是void* 指针
	class CThrdQueue {
	public:
		CThrdQueue(void);
		~CThrdQueue(void);

		//
		// 初始化队列
		// @param nSize 队列的大小，即队列满时最多存放的指针个数加1
		// @return 初始化成功返回true，失败返回false
		//
		bool Init(int nSize);

		//
		// 将指针放入队列尾
		// @param ptr 要放入队列的指针
		// @return 如果队列非满，放入成功，返回true，如果队列已满，放入失败，返回false
		// @remark 该函数只能被一个写线程调用，否则会出错，另外，请不要将空指针放入，
		//
		bool PushBack(void* ptr);

		//
		// 从队列头取出指针
		// @return 如果队列非空，则返回队列头的指针，如果队列空，返回NULL
		// @remark 该函数只能被一个读线程调用，否则会出错
		//
		void* PopFront();

		//
		// 是否是空
		// @return 空返回true,否则返回false
		// @remark 该函数只能被一个读线程调用，否则会出错
		//
		bool Empty()const;

	protected:
		void**  m_pArray;
		int		m_nHead;
		int		m_nTail;
		int		m_nSize;
	};
}