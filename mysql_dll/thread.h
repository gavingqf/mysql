#pragma once

// thread macro start.
#if (defined(_WIN32) || defined(WIN64))
  #define WIN32_LEAN_AND_MEAN
  #include "Windows.h"
  #include "process.h"
  #define THREAD_T HANDLE
  #define INIT_THREAD_ID NULL
  #define THREAD_FN unsigned int __stdcall
  #define THREAD_RETURN  0
  #define THREAD_START(threadvar, fn, arg) do {		                     \
	    uintptr_t threadhandle = _beginthreadex(NULL,0,fn,(arg),0,NULL); \
	    (threadvar) = (HANDLE) threadhandle;                             \
	} while (0)
  #define THREAD_JOIN(th) WaitForSingleObject(th, INFINITE)
  #define THREAD_EXIT(th, val) CloseHandle(th)
	typedef unsigned int(__stdcall* pThreadFunc)(void*);
#else
  #include <pthread.h>
  #define THREAD_T       pthread_t
  #define INIT_THREAD_ID 0
  #define THREAD_FN      void*
  #define THREAD_RETURN  NULL
  #define THREAD_START(threadvar, fn, arg) \
	    pthread_create(&(threadvar), NULL, fn, arg)
  #define THREAD_JOIN(th) pthread_join(th, NULL)
  #define THREAD_EXIT(th, val) pthread_exit(val)
	typedef void* (*pThreadFunc)(void*);
#endif
// thread macro end.

namespace SMysqlSpace {
	// c++ thread
	class CThread {
	public:
		enum ThreadStatus {
			READY,		// ready
			RUNNING,	// running
			EXITING,	// exiting
			EXIT,		// exit 
		};

	public:
		CThread();
		virtual ~CThread();

	public:
		virtual void stop();
		virtual void run();

	public:
		// start thread.
		void     start();

		// exit thread.
		void     exit(void* retval = NULL);

		// join thread.
		void     join();

	public:
		// get thread identifier
		THREAD_T getTID() const { return m_thread; }

		// get/set thread's status
		ThreadStatus getStatus()const { return m_Status; }
		void     setStatus(ThreadStatus status) { m_Status = status; }

		// thread function.
		static THREAD_FN MyThreadProcess(void* derivedThread);

	private:
		// thread identifier
		THREAD_T m_thread;

		// thread status
		ThreadStatus m_Status;
	};
}
