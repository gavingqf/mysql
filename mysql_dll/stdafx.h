#pragma once
/*
 * mysql utility functions.
 */

#if (defined(_WIN32) || defined(WIN64))
#include <stdio.h>
#include <tchar.h>
#include <Windows.h>
#pragma warning(disable:4996)
#else
#include "unistd.h"
#endif
#include "string.h"
#include "gnlogger.h"

namespace SMysqlSpace {
	inline void strSafeCopy(char* szSrc, int nSrcLen, const char* szDst) {
		if (nullptr == szDst || nullptr == szSrc || nSrcLen <= 0) {
			return;
		}

		int nDstSize = (int)strlen(szDst);
		int nRealSize = nDstSize > nSrcLen ? nSrcLen : nDstSize;
		strncpy(szSrc, szDst, nRealSize);
		szSrc[nRealSize] = '\0';
	}

	// array size.
	template <unsigned int N>
	inline void strSafeNCopy(char(&szSrc)[N], const char* szDst) {
		strSafeCopy(szSrc, N, szDst);
	}

	template <typename T>
	inline int ArraySize(const T& arData) {
		return sizeof(arData) / sizeof(arData[0]);
	}

	// logger
	extern GNDP::IGNLogger* gPoLogger;
	extern unsigned int gLoggerLevel;

	// critical
#define Critical \
	if (gPoLogger && gLoggerLevel & LOGLV_CRITICAL) gPoLogger->Critical

// info
#define Info     \
	if (gPoLogger && gLoggerLevel & LOGLV_INFO) gPoLogger->Info

// debug
#define Debug    \
	if (gPoLogger && gLoggerLevel & LOGLV_DEBUG) gPoLogger->Debug

// warn
#define Warn     \
	if (gPoLogger && gLoggerLevel & LOGLV_WARN) gPoLogger->Warn

// sleep
#if defined(WIN32) || defined(WIN64)
#define sleep Sleep
#else
#define sleep usleep
#endif
}
