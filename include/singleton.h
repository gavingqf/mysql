#ifndef __SINGLETON_H_20090524__
#define __SINGLETON_H_20090524__

#ifndef NULL
#define NULL 0
#endif

// singleton declare.
#define  DECLARE_SINGLETON(cls)\
private:\
	static cls* m_pInstance;\
	\
public:\
	static cls* Instance()\
	{\
	   return m_pInstance;\
	}\
	\
	static bool CreateInstance()\
	{\
	   if (NULL == m_pInstance)\
	   {\
	      m_pInstance = new cls;\
	   }\
	   return (NULL != m_pInstance);\
	}\
	\
	static void DestroyInstance()\
	{\
	   if (m_pInstance)\
	   {\
	      delete m_pInstance;\
	      m_pInstance = NULL;\
	   }\
	}

/// singleton implement
#define IMPLEMENT_SINGLETON(cls)\
	cls* cls::m_pInstance = NULL;



// a singleton class which can be inherited directly.
template <typename T>
class CSingleton
{
protected:
	CSingleton()          {}
	virtual ~CSingleton() {}
	CSingleton(const CSingleton& rhs) {}
	CSingleton& operator [] (const CSingleton& rhs) {}

public:
	static T* Instance()
	{
		return m_pTInstance;
	}

	static bool CreateInstance()
	{
		if (NULL == m_pTInstance)
		{
			m_pTInstance = new T;
		}

		return NULL != m_pTInstance;
	}

	static void DestroyInstance()
	{
		if (m_pTInstance)
		{
			delete m_pTInstance;
			m_pTInstance = NULL;
		}
	}

protected:
	static T* m_pTInstance;
};

// declare friend class.
#define DECLEARE_FRIENDS_CLASS(T) friend class CSingleton<T>

#endif