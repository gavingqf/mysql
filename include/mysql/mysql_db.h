#pragma once
/*
 * mysql global header.
 */

#include "../gnlogger.h"

#if defined(WIN32) || defined(WIN64)
    #ifndef STDCALL
    #define STDCALL __stdcall
    #endif
#else
    #define STDCALL
#endif

namespace SMysqlSpace {
   #define MAX_IP_LEN  16
   #define MAX_NAME_LEN 32
   #define MAX_PASSWORD_LEN 16
   #define MAX_DBNAME_LEN  32
   #define MAX_CHARSET_LEN 16
   #define MAX_CONNECT_POOL_SIZE 100

	// connection information
	typedef struct {
		char    ip[MAX_IP_LEN];
		unsigned short  wPort;
		char    user[MAX_NAME_LEN];
		char    password[MAX_PASSWORD_LEN];
		char    db[MAX_DBNAME_LEN];
		char    charset[MAX_CHARSET_LEN];
	} SConnectMysqlInfo;

	// error code
	enum EExecuteResult {
		// 未知错误
		MYSQL_EXECUTE_UNKNOWN = -5,

		// 没法连接错误
		MYSQL_CONNECT_ERROR = -4,

		// 索引错误
		MYSQL_EXECUTE_NO_INDEX = -3,

		// 参数错误
		MYSQL_EXECUTE_PARA_ERROR = -2,

		// 执行结果错误
		MYSQL_EXECUTE_RESULT_FAIL = -1,

		// 这个以上的表示出错.

		// 成功
        MYSQL_EXECUTE_RESULT_SUCC = 0,

		// 没有结果
		MYSQL_EXECUTE_RESULT_WITHOUT_RES = 1,

		// 有结果
        MYSQL_EXECUTE_RESULT_WITH_RES = 2,
	};

	// mysql record struct
	class IMysqlRecordset {
	public:
		virtual STDCALL ~IMysqlRecordset() {}

		// row number and field number
		virtual unsigned int STDCALL GetRowsNum()const  = 0;               
		virtual unsigned int STDCALL GetFieldsNum()const = 0;

		// next array
		virtual bool STDCALL Next() = 0;

		// index of get const char*, if does not exist, then return ""
		virtual const char* STDCALL GetFieldOfIndex(unsigned int dwIndex) const = 0;

		// field name of const char*
		virtual const char* STDCALL GetFieldOfName(const char *szField) const = 0;

		// get field length of dwIndex
		virtual int STDCALL GetFieldLength(unsigned int dwIndex) = 0;

		// release
		virtual void STDCALL Release() = 0;
	};

	// connect to mysql
	class IMysqlConnection {
	public:
		virtual STDCALL ~IMysqlConnection() {}

		// query return result: MYSQL_EXECUTE_RESULT_WITHOUT_RES(没有结果) and MYSQL_EXECUTE_RESULT_WITH_RES(有结果), 其他的表示出错.
		virtual int STDCALL QueryWithResult(const char* szSql, IMysqlRecordset** poRes) = 0; 

		// query return no result
		// result: error(< MYSQL_EXECUTE_RESULT_SUCC, 出错), else affect line(影响的行数)
		virtual int STDCALL QueryWithoutResult(const char* szSql) = 0;

		// escape string, should assert(nFromLen >= (2 * nToLen + 1)), otherwise, return -1
		virtual int STDCALL EscapeString(const char *szFrom, int nFromLen, char *szTo, int nToLen) = 0;

		// query last error, id 和 具体内容.
		virtual const char* STDCALL GetLastError() = 0;
		virtual int STDCALL GetLastErrorNo() = 0;
	};

	// sql command.
	class ICommand {
	public:
		virtual STDCALL ~ICommand() {}

		// thread execute function
		virtual void STDCALL OnExecuteSql(IMysqlConnection *poConnect) = 0;

		// main execute function
		virtual void STDCALL OnExecuted() = 0;

		// release object
		virtual void STDCALL Release() = 0;
	};

	// mysql module
	class IMysqlModule {
	public:
		virtual STDCALL ~IMysqlModule() {}

		// run the query result
		virtual bool STDCALL Run(int nCount = -1) = 0;

		// escape data
		virtual int STDCALL EscapeString(int nGroup, const char *szFrom, int nLen, char *szTo, int nToLen) = 0;
		virtual int STDCALL EscapeString(const char *szFrom, int nLen, char *szTo, int nToLen) = 0;

		// connect mysql, nConnectNum denote connect number
		virtual bool STDCALL Connect(int nGroup, const SConnectMysqlInfo &stConnectInfo, int nConnectNum = 1) = 0;

		// find connection
		virtual IMysqlConnection* STDCALL FindConnection(int nGroup, int nIndex) = 0;

		// add command
		virtual bool STDCALL AddCommand(int nGroup, int nIndex, ICommand *poCommand, bool bHighPriority = false) = 0;

		// query directly with or without result, 结果如同他IMysqlConnection.
		virtual int STDCALL QueryWithoutResult(int nGroup, int nIndex, const char *szSql) = 0;
		virtual int STDCALL QueryWithResult(int nGroup, int nIndex, const char *szSql, IMysqlRecordset **pRes) = 0;

		// query last error
		virtual const char* STDCALL GetLastError(int nGroup, int nIndex) = 0;
		virtual int STDCALL GetLastErrorNo(int nGroup, int nIndex) = 0;
		
		// release
		virtual void STDCALL Release() = 0;

		// close.
		virtual bool STDCALL Close(int nGroup) = 0;
	};

	// get mysql module GetMysqlModule
	IMysqlModule* STDCALL GetMysqlModule();

	// set logger
	void STDCALL SetMysqlLogger(GNDP::IGNLogger *poLogger, unsigned int dwLevel);
}