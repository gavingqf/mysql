#ifndef _INFO_STRUCT_H_
#define _INFO_STRUCT_H_

// packet dealer

#include <string>
#if defined(WIN32) || defined(WIN64)
#include "winsock2.h"
#else
  
#endif

#include "type.h"

using namespace std;

namespace SProtoSpace
{
	struct SFileInfo
	{
		string strName;
		string strContent;

		SFileInfo()
		{
			strName.clear();
			strContent.clear();
		}
	};

#define SDPKG_MARK16    0xAAEE
#define SDPKG_MARK32    0xAAEF

#pragma pack(push, 1)

	// protocol head
	struct SProtoHead
	{
		unsigned short wMark;  // mark
		int nDataLen;          // 协议长度
		int nCheckSum;         // 校验码
	};

	// packet data head
	struct SPacketDataHead
	{
		uint16 wMsgID;
		uint16 wFlag;
	};

#pragma pack(pop)

	// nLen表示接下来的协议长度
	inline int BuildPacketHead(SProtoHead &stHead, int nLen)
	{
		stHead.wMark	 = SDPKG_MARK16;
		stHead.nDataLen	 = nLen; // 除头之外的数据长度
		stHead.nCheckSum = (stHead.nDataLen ^ 0xBBCC) & 0x88AA;

		stHead.wMark	 = htons(stHead.wMark);
		stHead.nDataLen	 = htonl(stHead.nDataLen);
		stHead.nCheckSum = htonl(stHead.nCheckSum);

		return sizeof(SProtoHead);
	}

	inline int BuildProtoHead(SPacketDataHead &oProtoHead, uint16 wMsgId)
	{
		oProtoHead.wFlag = 1;
		oProtoHead.wMsgID = htons(wMsgId);
		return sizeof(SPacketDataHead);
	}

	// 检查是否有一个完整包
	inline int CheckPacket(const char *pszData, int nLen)
	{
		if (NULL == pszData || nLen <= 0)
		{
			return 0;
		}

		if (nLen < sizeof(SProtoHead))
		{
			return 0;
		}

		UINT16 wMark = ntohs(*(uint16*)pszData);
		if(SDPKG_MARK16 == wMark)
		{
			SProtoHead *pstProtoHead = (SProtoHead*)pszData;

			int nDataLen = ntohl(pstProtoHead->nDataLen);
			int nCheckSum = ntohl(pstProtoHead->nCheckSum);

			int nCalCheckSum = (nDataLen ^ 0xBBCC) & 0x88AA;
			if(nCheckSum != nCalCheckSum)
			{
				return -1;
			}

			int nCompletePackLen = nDataLen + (int)sizeof(SProtoHead);
			if (nLen < nCompletePackLen)
			{
				return 0;
			}
			else
			{
				return nCompletePackLen;
			}
		}
		else
		{
			return -1;
		}
	}

}

#endif


