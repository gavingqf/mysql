#ifndef __UTILITY_H__
#define __UTILITY_H__

#include "commonheader.h"
#include "type.h"
#include "time.h"

#if defined(WIN32) || defined(WIN64)
#include <sys/timeb.h>
// ansi to unicode
inline std::wstring Ansi2Unicode(const std::string& str)
{
	size_t len = str.length();
	int unicodeLen = ::MultiByteToWideChar(CP_ACP,
		0,
		str.c_str(),
		-1,
		NULL,
		0 );  

	wchar_t *pUnicode = new  wchar_t[unicodeLen + 1];  
	memset(pUnicode, 0, (unicodeLen+1) * sizeof(wchar_t));

	::MultiByteToWideChar( CP_ACP,
		0,
		str.c_str(),
		-1,
		(LPWSTR)pUnicode,
		unicodeLen );  
	std::wstring  rt;  
	rt = ( wchar_t* )pUnicode;

	delete  pUnicode; 

	return  rt;  
}

// unicode to ansi
inline std::string Unicode2Ansi(const std::wstring& str)
{
	int iTextLen = ::WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		NULL,
		0,
		NULL,
		NULL );

	char* pElementText = new char[iTextLen + 1];
	memset((void*)pElementText, 0, sizeof( char ) * (iTextLen + 1));

	::WideCharToMultiByte( CP_ACP,
		0,
		str.c_str(),
		-1,
		pElementText,
		iTextLen,
		NULL,
		NULL );
	std::string strText;
	strText = pElementText;
	delete[] pElementText;
	return strText;
}

// utf8 to unicode
inline std::wstring UTF82Unicode( const std::string& str )
{
	size_t len = str.length();
	int unicodeLen = ::MultiByteToWideChar( CP_UTF8,
		0,
		str.c_str(),
		-1,
		NULL,
		0 ); 

	wchar_t *pUnicode = new  wchar_t[unicodeLen+1];  
	memset(pUnicode, 0, (unicodeLen+1) * sizeof(wchar_t));  

	::MultiByteToWideChar( CP_UTF8,
		0,
		str.c_str(),
		-1,
		(LPWSTR)pUnicode,
		unicodeLen );

	std::wstring  rt = ( wchar_t* )pUnicode;
	delete pUnicode; 

	return rt;  
}

// unicode to utf8
inline std::string Unicode2UTF8( const std::wstring& str )
{
	int iTextLen = WideCharToMultiByte( CP_UTF8,
		0,
		str.c_str(),
		-1,
		NULL,
		0,
		NULL,
		NULL );

	char* pElementText = new char[iTextLen + 1];
	memset(( void* )pElementText, 0, sizeof(char) * ( iTextLen + 1 ));

	::WideCharToMultiByte( CP_UTF8,
		0,
		str.c_str(),
		-1,
		pElementText,
		iTextLen,
		NULL,
		NULL );

	std::string strText;
	strText = pElementText;
	delete[] pElementText;

	return strText;
}

inline std::string Utf8ToAnsi(const std::string &strData)
{
	return Unicode2Ansi(UTF82Unicode(strData));
}

inline std::string AnsiToUtf8(const std::string &strData)
{
	return Unicode2UTF8(Ansi2Unicode(strData));
}

#elif (LINUX)

#include "sys/time.h"
#include <stdlib.h>
#include <locale.h>

std::string GBKToUTF8(std::string strGBK)
{
	setlocale(LC_ALL, "zh_CN.gbk");
	size_t wdestlen = mbstowcs(NULL, strGBK.c_str(), 0);
	wchar_t* wdest = new wchar_t[wdestlen + 1];
	mbstowcs(wdest, strGBK.c_str(), wdestlen + 1);

	setlocale(LC_ALL, "zh_CN.utf8");
	size_t destlen = wcstombs(NULL, wdest, 0);
	char* dest = new char[destlen + 1];
	wcstombs(dest, wdest, destlen + 1);
	delete [] wdest;
	std::string strDest(dest);
	delete [] dest;
	setlocale(LC_ALL, "C");
	return strDest;
}

std::string UTF8ToGBK(std::string& strUTF8)
{
	setlocale(LC_ALL, "zh_CN.utf8");
	size_t wdestlen = mbstowcs(NULL, strUTF8.c_str(), 0);
	wchar_t* wdest = new wchar_t[wdestlen + 1];
	mbstowcs(wdest, strUTF8.c_str(), wdestlen + 1);

	setlocale(LC_ALL, "zh_CN.gbk");
	size_t destlen = wcstombs(NULL, wdest, 0);
	char* dest = new char[destlen + 1];
	wcstombs(dest, wdest, destlen + 1);
	delete [] wdest;
	std::string strDest(dest);
	delete [] dest;
	setlocale(LC_ALL, "C");
	return strDest;
}
#endif

inline std::string GetBinPath()
{
#ifndef MAX_PATH
#define MAX_PATH 260
#endif

	static char szPath[MAX_PATH] = {0};
	static bool g_bGet = false;

	if (false == g_bGet)
	{
#if defined(WIN32) || defined(WIN64)
		GetModuleFileName(NULL, szPath, MAX_PATH);
		char *pszPos = strrchr(szPath, '\\');
		if (NULL != pszPos)
		{
			*pszPos = '\0';
		}
#else
		return ".";
#endif
		g_bGet = true;
	}

#ifdef MAX_PATH
#undef MAX_PATH
#endif
	return szPath;
}

inline void StrNCpy(char *szSrc, int nSrcLen, const char *szDst)
{
	if (NULL == szDst || NULL == szSrc)
	{
		return ;
	}

	int nDstSize = (int)strlen(szDst);
	int nRealSize = nDstSize > nSrcLen ? nSrcLen : nDstSize;
	strncpy(szSrc, szDst, nRealSize);
	szSrc[nRealSize] = '\0';
}

template <unsigned int SIZE>
inline void StrNCpyEx(char (&szSrc)[SIZE], const char *szDst)
{
	StrNCpy(szSrc, SIZE, szDst);
}

template <typename T>
int ArraySize(const T &arArray)
{
	return sizeof(arArray) / sizeof(arArray[0]);
}

inline void ExtractStrings(const char* szStrData, char cDelim, 
						   std::vector<std::string> &vecData)
{
	string strTmp = szStrData;
	char *szStr = const_cast<char*>(strTmp.c_str());

	char* pStart = szStr;
	char* pszDelim = strchr(pStart, cDelim);
	while (NULL != pszDelim)
	{
		vecData.push_back(string(pStart, pszDelim - pStart));

		pStart = pszDelim;
		pszDelim = strchr(++pStart, cDelim);
	}

	if (NULL != pStart)
	{
		vecData.push_back(pStart);
	}
}

inline void StrSplit(const char *szData, const char *szDelim, 
					 std::vector<string> &vecData)
{
	string strTmp = szData;
	char *szStr = const_cast<char*>(strTmp.c_str());
	char *pszPos = strtok(szStr, szDelim);
	while (NULL != pszPos)
	{
		vecData.push_back(pszPos);
		pszPos = strtok(NULL, szDelim);
	}
}

inline uint64 GetSysTime(bool bLocal = true)
{
#if defined(WIN32) || defined(WIN64)
	__timeb64 stSysTime;
	_ftime64_s(&stSysTime);

	int nLocalSecond = 0;
	if (bLocal)
	{
		nLocalSecond = stSysTime.timezone * 60 * 1000;
	}
	else
	{
		nLocalSecond = 0;
	}
	return stSysTime.time * 1000 + stSysTime.millitm + nLocalSecond;
#else
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
#endif
}

// return 时间对应的秒数, 好像没有时区
inline time_t GetRelatedTime(int nYear, int nMonth, int nDay, 
							 int nHour, int nMinute, int nSecend)
{
	struct tm oTime;
	oTime.tm_yday = nYear - 1900;
	oTime.tm_mon = nMonth - 1;
	oTime.tm_mday = nDay;
	oTime.tm_hour = nHour;
	oTime.tm_min = nMinute;
	oTime.tm_sec = nSecend;

	return mktime(&oTime);
}

#endif