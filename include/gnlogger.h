#ifndef _GNLOGGER_20091208_H_
#define _GNLOGGER_20091208_H_

#include "gnbase.h"

namespace GNDP {
#define MAX_DATABUF_LEN  1024

#ifdef WIN32
#ifndef GNAPI
#define GNAPI __stdcall
#endif
#endif
	
	class IGNLogger
	{
	public:
		virtual ~IGNLogger() {}

		// 初始化函数，bAuto=true就在屏幕打印信息，否则不打印
		virtual bool GNAPI Init(const char* szFileName, bool bAuto = false) = 0;
        
		// 这种级别的信息输出
		virtual void GNAPI Warn(const char* szData, ...) = 0;
		virtual void GNAPI Debug(const char* szData, ...) = 0;
		virtual void GNAPI Critical(const char* szData, ...) = 0;
		virtual void GNAPI Info(const char* szData, ...) = 0;
		virtual void GNAPI Release() = 0;
	};

    // 获取日志模块, bOnScreen=true显示在屏幕上
	IGNLogger* GNAPI GetGNLogger(const char* szFileName, bool bAuto = false);
}

#endif