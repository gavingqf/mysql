#ifndef _pool_guard_h_
#define _pool_guard_h_

#include <vector>


class IPoolInterface {
public:
	virtual ~IPoolInterface() {}

	// all alloc size
	virtual int GetAllAllocSize() const = 0;

	// left alloc size
	virtual int GetLeftAllocSize() const = 0;

	// object name
	virtual const char *GetObjectName() const = 0;

	// use object size
	virtual int GetUsedSize() const = 0;

	// init size
	virtual int GetInitSize() const = 0;

	// grow size
	virtual int GetGrowSize() const = 0;

	// object size
	virtual int GetAllocMemorySize() const = 0;
};

typedef std::vector<IPoolInterface*> CPoolInterfaceVec;

class CPoolGuard {
public:
	static CPoolGuard &Instance() {
		static CPoolGuard g_oInstance;
		return g_oInstance;
	}

protected:
	CPoolGuard() {
		m_vecPool.clear();
	}
	~CPoolGuard() {
		m_vecPool.resize(0);
	}
	CPoolGuard(const CPoolGuard &rhs);
	CPoolGuard &operator =(const CPoolGuard &rhs);

public:
	void Register(IPoolInterface* poPool) {
		if (poPool) {
		    m_vecPool.push_back(poPool);
		}
	}
	const CPoolInterfaceVec &GetPoolVec() const {
		return m_vecPool;
	}

protected:
	CPoolInterfaceVec m_vecPool;
};


#endif