#ifndef __GNDP_BASE_H_20070601__
#define __GNDP_BASE_H_20070601__

#include "gntype.h"

namespace GNDP {

#ifdef WIN32
    #ifndef GNAPI
	    #define GNAPI __stdcall
    #endif
#else
    #define GNAPI
#endif
	// 版本信息
	struct SGNDPVersion
	{
		UINT16 wMajorVersion;
		UINT16 wMinorVersion;
		UINT16 wCompatibleVersion;
		UINT16 wBuildNumber;
	};

	const SGNDPVersion GNDP_VERSION = {2, 2, 4, 7};

	// 基本对象模块
	class IGNBase
	{
	public:
		virtual GNAPI ~IGNBase() {}

		// 引用计数增加1
		virtual void GNAPI AddRef(void) = 0;

		// 查询计数
		virtual UINT32  GNAPI QueryRef(void) = 0;

		// 释放
		virtual void GNAPI Release(void) = 0;

		// 获得版本
		virtual SGNDPVersion GNAPI GetVersion(void) = 0;

		// 获取module名
		virtual const char * GNAPI GetModuleName(void) = 0;
	};


	// 日志等级
#define LOGLV_NONE      0x0000
#define LOGLV_DEBUG     0x0001
#define LOGLV_INFO      (0x0001<< 1)
#define LOGLV_WARN      (0x0001<< 2)
#define LOGLV_CRITICAL  (0x0001<< 3)

	// 日志模块
	class ISDLogger
	{
	public:
		virtual GNAPI ~ISDLogger() = 0;

		// 写入文本数据
		virtual bool GNAPI LogText(const char *pszLog) = 0;

		// 写入二进制数据
		virtual bool GNAPI LogBinary(const UINT8 *pLog, UINT32 dwLen) = 0;
	};

}

#endif
